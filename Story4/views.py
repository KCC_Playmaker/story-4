from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def profile(request):
    return render(request,'Profile.html')

def home(request):
    return render(request,'Home.html')

def registerForm(request):
    return render(request,'RegisterForm.html')