from django.conf.urls import url
from django.urls import path
from .views import profile, home, registerForm

urlpatterns = [
    path('profile', profile, name='Profile'),
    path('', home, name='Home'),
    path('home', home, name='Home'),
    path('registerForm', registerForm, name='Register Form'),
]